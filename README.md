> notice: 这是github一个项目，能够进行Windows鼠标样式文件和Linux鼠标样式文件的互相转化。
> 
> **我做了一些补充，让它可以把 windows 的鼠标样式文件转换成 gif 文件，从而可以供给 macOS 的 Mousecape 使用。需要注意的是 gif 文件是没有半透明的状态的，如果鼠标设计本身有半透明的效果，需要合成png长图，下文中也提供了相应方法。**
> 
> 如果觉得原项目不错，可以给原项目点个星。

# 初心
做这个的原因是因为 macOS 的 Mousecape 不支持ani文件的导入，所以想把ani转成gif。网上呢也有其他的一些方法：比如[在线转换](https://ezgif.com/ani-to-gif)、鼠标样式文件编辑工具(reaConverter)。但是这两种方法都不能批量处理，在线转换我尝试过使用爬虫技术去破解它的接口，但是失败了。所以我最后决定做一个离线的。费了好大的功夫才找到了这个项目可以去解析ani的格式。然后也顺利地把动画帧抽出来。希望能给喜欢装饰macOS的朋友提供一点帮助。

# `win2xcur` and `x2wincur` [![Build Status](https://img.shields.io/github/workflow/status/quantum5/win2xcur/Python%20package)](https://github.com/quantum5/win2xcur/actions) [![PyPI](https://img.shields.io/pypi/v/win2xcur.svg)](https://pypi.org/project/win2xcur/) [![PyPI - Format](https://img.shields.io/pypi/format/win2xcur.svg)](https://pypi.org/project/win2xcur/) [![PyPI - Python Version](https://img.shields.io/pypi/pyversions/win2xcur.svg)](https://pypi.org/project/win2xcur/)

`win2xcur` is a tool that converts cursors from Windows format (`*.cur`,
`*.ani`) to Xcursor format. This allows Windows cursor themes to be used on
Linux, for example.

`win2xcur` is more than a simple image conversion tool. It preserves the cursor
hotspot and animation delay, and has an optional mode to add shadows that
replicates Windows's cursor shadow effect.

`x2wincur` is a tool that does the opposite: it converts cursors in the Xcursor
format to Windows format (`*.cur`, `*.ani`), allowing to use your favourite
Linux cursor themes on Windows.

`wincur2img` 是一个Windows鼠标样式文件转普通图片的工具。目前实现了ani文件转gif、批量的png文件和png长图。如果有其他需求可以提一下issue，看到会尽量处理。

## Installation
### 原项目的安装教程
To install the latest stable version:

    pip install win2xcur

To install from GitHub:

    pip install -e git+https://github.com/quantum5/win2xcur.git

### 当前项目的安装教程
当前项目并没有推到pip的平台上，所以只能通过Gitee安装
```sh
pip install -e git+https://gitee.com/xuanyusan/win2xcur_plus.git#egg=win2xcur_plus
```
或者
```sh
git clone https://gitee.com/xuanyusan/win2xcur_plus.git
python setup.py build
python setup.py install
```

## Usage: `win2xcur`

For example, if you want to convert [the sample cursor](sample/crosshair.cur)
to Linux format:

    mkdir output/
    win2xcur sample/crosshair.cur -o output/

`-s` can be specified to enable shadows.
Multiple cursors files can be specified on the command line.
For example, to convert a directory of cursors with shadows enabled:

    win2xcur input/*.{ani,cur} -o output/ 

For more information, run `win2xcur --help`.

## Usage: `x2wincur`

For example, if you want to convert DMZ-White to Windows:

    mkdir dmz-white/
    x2wincur /usr/share/icons/DMZ-White/cursors/* -o dmz-white/

## Usage: `wincur2img`

example 目录下我放了一个例子:

```sh
# 转换成一张gif图片
wincur2img ./忙.ani -o output.gif
# 分解成多张png图片
wincur2img ./忙.ani -o output.gif --type png
# 合成一张png长图，最大是24帧，所以大于24的还做了抽帧
wincur2img ./忙.ani -o output.gif --type apng
```

## Troubleshooting

`win2xcur` and `x2wincur` should work out of the box on most systems. If you
are using unconventional distros (e.g. Alphine) and are getting errors related
to `wand`, please see the [Wand documentation on installation][wand-install].

  [wand-install]: https://docs.wand-py.org/en/0.6.7/guide/install.html
